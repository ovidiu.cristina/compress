#include "ops.h"

op getOp(const char *s){
    if(!strcmp(s,"-c")) return COMPRESS;
    if(!strcmp(s,"-d")) return DECOMPRESS;
    return INVALID;
}

void compress(const char *fi_path,const char *fo_path){
    bytes *b1;
    huffTree *ht;
    bitset *msg;
    file *fi,*fo;
    int aux;

    fi = initFile(fi_path,"r");
    ht = initHuffTree(fi);

    fo = initFile(fo_path,"w");
    writeHuffTree(ht,fo);

    do{
        b1 = readBytesFromFile(fi);

        msg = encode(b1,ht);
        writeBitset(msg,fo);

        aux = b1->size;
        
        delBitset(msg);
        delBytes(b1);
    }while(aux == BLOCK_DIM);
    
    delHuffTree(ht);
    delFile(fi);
    delFile(fo);
}

void decompress(const char *fi_path,const char *fo_path){
    bytes *b1;
    huffTree *ht;
    bitset *msg;
    file *fi,*fo;
    int aux;

    fo = initFile(fo_path,"w");

    fi = initFile(fi_path,"r");
    ht = readHuffTree(fi);

    do{
        msg = readBitset(fi);
        b1 = decode(msg,ht);
        writeBytesToFile(b1,fo);

        aux = b1->size;

        delBitset(msg);
        delBytes(b1);
    }while(aux == BLOCK_DIM);

    delHuffTree(ht);
    delFile(fi);
    delFile(fo);
}
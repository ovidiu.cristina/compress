#include "huff.h"

static void swap(huff **n1,huff **n2){
    huff *tmp = *n1;
    *n1 = *n2;
    *n2 = tmp;
}

pq *initPQ(int (*cmp)(const huff*,const huff*)){
    pq *ans = malloc(sizeof(pq));
    assert(ans != NULL);

    ans -> cmp = cmp;
    ans -> cap = BLOCK_SIZE;
    ans -> size = 0;
    ans -> data = malloc(ans -> cap * sizeof(huff*));
    assert(ans->data != NULL);
    
    return ans;
}

static void extendPQ(pq *p){
    p->cap += BLOCK_SIZE;

    p->data = realloc(p->data,p->cap * sizeof(huff*));

    assert(p->data != NULL);
}

void delPQ(pq *p){
    free(p->data);
    free(p);
}

static void heapifyPQ(pq *p,int i){
    int great = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if(left < p->size && p->cmp(p->data[left],p->data[great]) > 0){
        great = left;
    }

    if(right < p->size && p->cmp(p->data[right],p->data[great]) > 0){
        great = right;
    }

    if(great != i){
        swap(p->data + i,p->data + great);
        heapifyPQ(p,great);
    }
}

static void heapifyLeafPQ(pq *p,int i){
    if(i == 0) return;

    int parent = i / 2;

    if(p->cmp(p->data[i],p->data[parent]) > 0){
        swap(p->data + i,p->data + parent);
        heapifyLeafPQ(p,parent);
    }
}

void addPQ(pq *p,huff *x){    
    if(p->size == p->cap) extendPQ(p);

    p->data[p->size] = x;

    heapifyLeafPQ(p,p->size);
    ++p->size;
}

huff *remPQ(pq *p){
    if(p->size == 0) return NULL;

    huff *ans = p->data[0];

    p->data[0] = p->data[--p->size];

    heapifyPQ(p,0);

    return ans;
}

void printPQ(pq *p){
    for(int i = 0;i < p->size;++i){
        printHuff(p->data[i]);
    }
    putchar('\n');
}
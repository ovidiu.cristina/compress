#include "bitset.h"

#define setBit(n,p)     ((n) |= (1 << (p)))
#define toggleBit(n,p)  ((n) ^= (1 << (p)))
#define resetBit(n,p)   ((n) &= (~(1 << (p))))
#define getBit(n,p)     ((n) & (1 << (p)))

static bitset *initBitsetWithCap(int cap){
    bitset *ans = malloc(sizeof(bitset));
    assert(ans != NULL);

    ans->size = 0;
    ans->cap = cap;

    ans->b = malloc(ans->cap);
    assert(ans->b != NULL);

    return ans;
}

bitset *initBitset(){
    return initBitsetWithCap(0);
}

bitset *initBitsetFromBytes(bytes *by){
    bitset *ans = initBitsetWithCap(by->cap);
    memcpy(ans->b,by->data,by->cap);

    ans->cap = by->cap;
    ans->size = ans->cap * 8;

    return ans;
}

void expandBitset(bitset *bs){
    ++bs->cap;
    bs->b = realloc(bs->b,bs->cap);
}

void addBitset(bitset *bs,bool bit){
    if(bs->size / 8 == bs->cap) expandBitset(bs);

    if(bit) setBit(bs->b[bs->size / 8],bs->size % 8);
    else    resetBit(bs->b[bs->size / 8],bs->size % 8);

    ++bs->size;
}

bool getBitset(const bitset *bs,int pos){
    return getBit(bs->b[pos / 8],pos % 8) ? true : false;
}

void delBitset(bitset *bs){
    if(bs == NULL) return;

    free(bs->b);
    free(bs);
}

bitset *dupBitset(const bitset *bs){
    bitset *ans = malloc(sizeof(bitset));
    assert(ans != NULL);

    ans->cap = bs->cap;
    ans->size = bs->size;

    ans->b = malloc(ans->cap);
    assert(ans->b != NULL);
    
    memcpy(ans->b,bs->b,ans->cap);

    return ans;
}

void printBitset(const bitset *bs){
    for(int i = 0;i < bs->size;++i){
        putchar(getBitset(bs,i) ? '1' : '0');
        if((i + 1) % 8 == 0) putchar(' ');
    }
    putchar('\n');
}

void appendToBitset(bitset *to,const bitset *what){
    for(int i = 0;i < what->size; ++i){
        addBitset(to,getBitset(what,i));
    }
}

void writeBitset(bitset *bs,file *fl){
    fwriteFile(&bs->cap,sizeof(bs->cap),fl);
    fwriteFile(&bs->size,sizeof(bs->size),fl);

    if(bs->cap > 0) fwriteFile(bs->b,bs->cap,fl);

    fl->size += sizeof(bs->cap) + sizeof(bs->size) + bs->cap;
}

bitset *readBitset(file *fl){
    int cap;
    freadFile(&cap,sizeof(cap),fl);
    bitset *bs = initBitsetWithCap(cap);
    freadFile(&bs->size,sizeof(bs->size),fl);

    if(bs->cap > 0) freadFile(bs->b,bs->cap,fl);
    
    return bs;
}
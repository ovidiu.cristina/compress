#include "file.h"

file *initFile(const char *path,const char *mode){
    file *ans = malloc(sizeof(file));
    assert(ans != NULL);

    ans->f = fopen(path,mode);
    if(ans->f == NULL) err("fopen(): ");

    fseek(ans->f,0,SEEK_END);
    ans->size = ftell(ans->f);
    fseek(ans->f,0,SEEK_SET);
    
    ans->pos = 0;

    return ans;
}

long remainingFile(file *fl){
    return fl->size - fl->pos;
}

void resetFile(file *fl){
    fseek(fl->f,0,SEEK_SET);
    fl->pos = 0;
}

void freadFile(void *buf,size_t size,file *fl){
    if(!fread(buf,size,1,fl->f))  err("fread(): ");

    fl->pos += size;
}

void fwriteFile(void *buf,size_t size,file *fl){
    if(!fwrite(buf,size,1,fl->f))  err("fwrite(): ");

    fl->size += size;
}

void delFile(file *fl){
    fclose(fl->f);
    free(fl);
}
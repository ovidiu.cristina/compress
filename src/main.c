#include <stdio.h>
#include <string.h>
#include <time.h>
#include "ops.h"

int main(int argc,char **argv){
    if(argc < 4){
        printf("Usage: %s <mode> <infile> <outfile>\n",argv[0]);
        return 1;
    }

    clock_t beg,end;

    switch (getOp(argv[1]))
    {
    case COMPRESS:
        beg = clock();
        compress(argv[2],argv[3]);
        end = clock();
        
        printf("Compression took %f sec\n",(double)(end - beg) / CLOCKS_PER_SEC);
        break;
    
    case DECOMPRESS:
        beg = clock();
        decompress(argv[2],argv[3]);
        end = clock();
        
        printf("Decompression took %f sec\n",(double)(end - beg) / CLOCKS_PER_SEC);
        break;

    default:
        printf("Invalid option!\n");
        return 1;
    }
    
    return 0;
}
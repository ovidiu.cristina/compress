#include "bytes.h"

static bytes *initBytesWithSize(int size){
    bytes *ans = malloc(sizeof(bytes));
    assert(ans != NULL);

    ans->size = 0;
    ans->cap = (size / EXPAND_BLOCK + 1) * EXPAND_BLOCK;

    ans->data = malloc(ans->cap);
    assert(ans->data != NULL);

    return ans;
}

bytes *initBytes(){
    return initBytesWithSize(0);
}

bytes *readBytesFromFile(file *fl){
    long size = remainingFile(fl);
    if(size > BLOCK_DIM) size = BLOCK_DIM;

    bytes *ans = initBytesWithSize(size);
    ans->size = size;
    
    if(size > 0) freadFile(ans->data,size,fl);

    return ans;
}

void writeBytesToFile(const bytes *bt,file *fl){
    if(bt->size > 0) fwriteFile(bt->data,bt->size,fl);
}

bytes *initBytesFromString(const char *s){
    int len = strlen(s);

    bytes *ans = malloc(sizeof(bytes));
    assert(ans != NULL);
    
    ans->size = len;
    ans->cap = (len / EXPAND_BLOCK + 1) * EXPAND_BLOCK;

    ans->data = malloc(ans->cap);
    assert(ans->data != NULL);

    memcpy(ans->data,s,len);

    return ans;
}

static void expandBytes(bytes *bt){
    bt->cap += EXPAND_BLOCK;
    bt->data = realloc(bt->data,bt->cap);
    assert(bt->data != NULL);
}

void addChar(bytes *bt,char c){
    if(bt->size == bt->cap) expandBytes(bt);

    bt->data[bt->size++] = c;
}

void delBytes(bytes *bt){
    free(bt->data);
    free(bt);
}

void printBytes(bytes *bt){
    for(int i = 0;i < bt->size;++i){
        putchar(bt->data[i]);
    }
    putchar('\n');
}
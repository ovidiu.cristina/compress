#include "huff.h"

#define R   256

static bool isLeaf(huff *tree);
static void _getCodes(bitset **codes,huff *tree,bitset *current);
static bitset **getCodes(huff *tree);
static void delCodes(bitset **codes);
static int cmpHuffGreater(const huff *x1,const huff *x2);
static int cmpHuffLower(const huff *x1,const huff *x2);

static huff *initHuff(char ch,int freq,huff *left,huff *right){
    huff *ans = malloc(sizeof(huff));
    assert(ans != NULL);

    ans->ch = ch;
    ans->freq = freq;
    ans->left = left;
    ans->right = right;

    return ans;
}

static void delHuff(huff *x){
    if(x == NULL) return;

    delHuff(x->left);
    delHuff(x->right);
    free(x);
}

static int cmpHuffGreater(const huff *x1,const huff *x2){
    return x1->freq - x2->freq;
}

static int cmpHuffLower(const huff *x1,const huff *x2){
    return -cmpHuffGreater(x1,x2);
}

void printHuff(const huff *x){
    printf("||%c:%d|| ",x->ch,x->freq);
}

static int *getFreq(file *fl){
    int *freq = calloc(R,sizeof(int));
    assert(freq != NULL);

    int aux;
    bytes *s;
    do{
        s = readBytesFromFile(fl);
        for(int i = 0;i < s->size;++i){
            ++freq[(unsigned char)s->data[i]];
        }
        aux = s->size;
        delBytes(s);
    }while(aux == BLOCK_DIM);

    resetFile(fl);

    return freq;
}

huffTree *initHuffTree(file *fl){
    int *freq = getFreq(fl);

    pq *p = initPQ(cmpHuffLower);

    for(int i = 0;i < R;++i){
        if(freq[i] != 0){
            addPQ(p,initHuff((char)i,freq[i],NULL,NULL));
        }
    }

    free(freq);

    while(p->size > 1){
        huff *l = remPQ(p);
        huff *r = remPQ(p);

        addPQ(p,initHuff(0,l->freq + r->freq,l,r));
    }

    huffTree *ans = malloc(sizeof(huffTree));
    assert(ans != NULL);

    ans->tree = remPQ(p);
    ans->codes = getCodes(ans->tree);

    return ans;
}

void delHuffTree(huffTree *ht){
    delCodes(ht->codes);
    delHuff(ht->tree);
    free(ht);
}

static bool isLeaf(huff *tree){
    return tree->left == NULL && tree->right == NULL;
}

static bitset **getCodes(huff *tree){
    bitset **ans = calloc(R,sizeof(bitset*));
    assert(ans != NULL);
    
    _getCodes(ans,tree,initBitset());
    return ans;
}

static void _getCodes(bitset **codes,huff *tree,bitset *current){
    if(isLeaf(tree)){
        codes[(unsigned char)tree->ch] = dupBitset(current);
    }else {
        bitset *aux1 = dupBitset(current);
        bitset *aux2 = dupBitset(current);
        addBitset(aux1,false);
        addBitset(aux2,true);

        _getCodes(codes,tree->left,aux1);
        _getCodes(codes,tree->right,aux2);
    }
    delBitset(current);
}

static void delCodes(bitset **codes){
    for(int i = 0;i < R;++i){
        delBitset(codes[i]);
    }
    free(codes);
}

bitset *encode(const bytes *message,huffTree *ht){
    bitset *ans = initBitset();
    for(int i = 0;i < message->size;++i){
        appendToBitset(ans,ht->codes[(unsigned char)message->data[i]]);
    }

    return ans;
}

bytes *decode(const bitset *encoded,huffTree *ht){
    bytes *ans = initBytes();
    
    huff *x = ht->tree;
    for(int i = 0;i < encoded->size;++i){
        if(getBitset(encoded,i))    x = x->right;
        else                        x = x->left;

        if(isLeaf(x)){
            addChar(ans,x->ch);
            x = ht->tree;
        }
    }
    
    return ans;
}

static void writeHuff(huff *x,file *fl){
    if(x == NULL){
        char ch = 0;
        int freq = -1;
        fwriteFile(&ch,sizeof(ch),fl);
        fwriteFile(&freq,sizeof(freq),fl);
        return;
    }

    fwriteFile(&x->ch,sizeof(x->ch),fl);
    fwriteFile(&x->freq,sizeof(x->freq),fl);

    writeHuff(x->left,fl);
    writeHuff(x->right,fl);
}

static huff *readHuff(file *fl){
    char ch;
    int freq;

    freadFile(&ch,sizeof(ch),fl);
    freadFile(&freq,sizeof(freq),fl);

    if(ch == 0 && freq == -1) return NULL;

    huff *left = readHuff(fl);
    huff *right = readHuff(fl);

    return initHuff(ch,freq,left,right);
}

void writeHuffTree(huffTree *ht,file *fl){
    writeHuff(ht->tree,fl);
}

huffTree *readHuffTree(file *fl){
    huffTree *ht = malloc(sizeof(huffTree));
    assert(ht != NULL);

    ht->tree = readHuff(fl);
    ht->codes = getCodes(ht->tree);

    return ht;
}
TARGET = compress.x
hdr = $(wildcard include/*.h)
src = $(wildcard src/*.c)
obj = $(patsubst src/%.c,obj/%.o,$(src))

obj/%.o:src/%.c
	gcc -Wall -I ./include -c $^ -o $@

all:dir $(obj) $(src) $(hdr)
	gcc -Wall $(obj) -o $(TARGET) -g
	
dir:
	mkdir -p obj/

clear:
	rm -rf obj/
	rm -f $(TARGET)
	
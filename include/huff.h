#ifndef __HUFF_H
#define __HUFF_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include "bytes.h"
#include "bitset.h"
#include "utils.h"

#define BLOCK_SIZE  64

typedef struct _huff{
    char ch;
    int freq;

    struct _huff *left,*right;
}huff;

void printHuff(const huff *x);

typedef struct{
    huff *tree;
    bitset **codes;
}huffTree;

huffTree *initHuffTree(file *fl);
void delHuffTree(huffTree *ht);
bitset *encode(const bytes *message,huffTree *ht);
bytes *decode(const bitset *encoded,huffTree *ht);
void writeHuffTree(huffTree *ht,file *fl);
huffTree *readHuffTree(file *fl);

//PQ
typedef struct{
    huff **data;
    int size,cap;

    int(*cmp)(const huff*,const huff*);
}pq;

pq *initPQ(int (*cmp)(const huff*,const huff*));
void delPQ(pq *p);
void addPQ(pq *p,huff *x);
huff *remPQ(pq *p);
void printPQ(pq *p);

#endif
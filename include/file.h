#ifndef __FILE_H
#define __FILE_H

#define BLOCK_DIM   4096

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "utils.h"

typedef struct{
    FILE *f;
    long pos,size;
}file;

file *initFile(const char *path,const char *mode);
long remainingFile(file *fl);
void freadFile(void *buf,size_t size,file *fl);
void fwriteFile(void *buf,size_t size,file *fl);
void resetFile(file *fl);
void delFile(file *fl);

#endif
#ifndef __OPS_H
#define __OPS_H

#include <stdio.h>
#include <string.h>
#include "huff.h"
#include "bitset.h"
#include "utils.h"

typedef enum{
    COMPRESS,
    DECOMPRESS,
    INVALID,
}op;

op getOp(const char *s);
void compress(const char *fi_path,const char *fo_path);
void decompress(const char *fi_path,const char *fo_path);

#endif
#ifndef __BYTES_H
#define __BYTES_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "utils.h"
#include "file.h"

#define EXPAND_BLOCK    64

typedef struct{
    unsigned char *data;
    int size,cap;
}bytes;

bytes *initBytes();
bytes *initBytesFromString(const char *s);
bytes *readBytesFromFile(file *fl);
void writeBytesToFile(const bytes *bt,file *fl);
void delBytes(bytes *bt);
void addChar(bytes *bt,char c);
void printBytes(bytes *bt);

#endif
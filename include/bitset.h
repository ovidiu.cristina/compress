#ifndef __BITSET_H
#define __BITSET_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "utils.h"
#include "bytes.h"

typedef struct{
    unsigned char *b;
    int size,cap;
}bitset;

bitset *initBitset();
bitset *initBitsetFromBytes(bytes *by);
void expandBitset(bitset *bs);
void addBitset(bitset *bs,bool bit);
bool getBitset(const bitset *bs,int pos);
void delBitset(bitset *bs);
bitset *dupBitset(const bitset *bs);
void printBitset(const bitset *bs);
void appendToBitset(bitset *to,const bitset *what);
void writeBitset(bitset *bs,file *fl);
bitset *readBitset(file *fl);

#endif
#ifndef __UTILS_H
#define __UTILS_H
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#define offsetof(type,memb)     sizeof(((type*)0)->memb)

void err(const char *str);

#endif